/* eslint-disable */
export const state = () => ({
  projects: []
});

export const getters = {
  getProjects: state => state.projects,
  getFavProjects: state => {
    return state.projects.filter(a => a.is_favorites);
  },
  authorProjects: state => {
    return state.projects.filter(a => a.owner);
  },
  participantProjects: state => {
    return state.projects.filter(a => !a.owner);
  },
  finishedProjects: state => {
    return state.projects.filter(a => a.status === 3);
  },
  stoppedProjects: state => {
    return state.projects.filter(a => a.status === 2);
  },
  runningProjects: state => {
    return state.projects.filter(a => a.status === 1);
  }
};

export const actions = {
  async unFaveProject({ commit }, project) {
    await commit("UN_FAVE_PROJECT", project);
  },
  async faveProject({ commit }, project) {
    await commit("FAVE_PROJECT", project);
  },
  async deleteProject({ commit }, project) {
    await commit("DELETE_PROJECT", project);
  },
  async loadData({ commit }) {
    const res = await this.$axios.get("./project.json");
    const data = res.data;
    console.log(res.data)
    data.forEach(function(a) {
      const date = new Date(a.last_changed)
      const year = date.getFullYear();
      let month = date.getMonth() + 1;
      let dt = date.getDate();
      if (dt < 10) {
        dt = "0" + dt;
      }
      if (month < 10) {
        month = "0" + month;
      }
      a.last_changed = dt + '.' + month + ' ' + year
    });
    commit("ADD_DATA", data);
  },
  addNew({ commit }, newProject) {
    commit("ADD_PROJECT", newProject);
  }
};

export const mutations = {
  UN_FAVE_PROJECT(state, project) {
    const pr = state.projects.find(a => a.id_project === project.id_project);
    pr.is_favorites = false;
  },
  FAVE_PROJECT(state, project) {
    const pr = state.projects.find(a => a.id_project === project.id_project);
    pr.is_favorites = true;
  },
  DELETE_PROJECT(state, project) {
    state.projects = state.projects.filter(
      a => a.id_project !== project.id_project
    );
  },
  ADD_DATA(state, data) {
    state.projects.forEach(function(a) {
      a.menu = true;
    });
    state.projects.forEach(function(a) {
      a.overlay = false;
    });
    state.projects = data;
  },
  ADD_PROJECT(state, newProject) {
    state.projects.push(newProject);
  }
};
